<?php

namespace Newebtime\JoomlamigratorModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

/**
 * Class JoomlamigratorModule
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class JoomlamigratorModule extends Module
{
    /**
     * {@inheritdoc}
     */
    protected $navigation = true;

    /**
     * {@inheritdoc}
     */
    protected $icon = 'fa fa-puzzle-piece';

    /**
     * {@inheritdoc}
     */
    protected $sections = [
        'migrator' => [
            'buttons' => [
                'settings' => [
                    'enabled' => 'admin/joomlamigrator',
                ],
            ],
            'sections' => [
                'settings' => [
                    'hidden'  => true,
                ],
            ],
        ],
        'help' => [],
    ];
}
