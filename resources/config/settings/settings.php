<?php

return [
    'database_host'        => [
        'required' => true,
        'type'     => 'anomaly.field_type.text',
        'config'   => [
            'default_value' => 'localhost'
        ]
    ],
    'database_name'        => [
        'required' => true,
        'type'     => 'anomaly.field_type.text',
    ],
    'database_prefix'      => [
        'type' => 'anomaly.field_type.text',
    ],
    'database_user'        => [
        'required' => true,
        'type'     => 'anomaly.field_type.text',
    ],
    'database_password'    => [
        'type' => 'anomaly.field_type.encrypted',
    ],
    'migrate_com_users'    => [
        'type'   => 'anomaly.field_type.boolean',
        'config' => [
            'default_value' => true,
        ],
    ],
    'migrate_com_content'  => [
        'type'   => 'anomaly.field_type.boolean',
        'config' => [
            'default_value' => true,
        ],
    ],
    'migrate_com_contact'  => [
        'type'   => 'anomaly.field_type.boolean',
        'config' => [
            'default_value' => true,
        ],
    ],
    'migrate_com_menus'    => [
        'type'   => 'anomaly.field_type.boolean',
        'config' => [
            'default_value' => true,
        ],
    ],
    'migrate_com_redirect' => [
        'type'   => 'anomaly.field_type.boolean',
        'config' => [
            'default_value' => true,
        ],
    ],
    'migrate_content_to'   => [
        'required' => true,
        'type'     => 'anomaly.field_type.select',
        'config'   => [
            'default_value' => 'auto',
            'options'       => [
                'auto' => 'newebtime.module.joomlamigrator::setting.migrate_content_to.option.automatic',
                'page' => 'newebtime.module.joomlamigrator::setting.migrate_content_to.option.page',
                'post' => 'newebtime.module.joomlamigrator::setting.migrate_content_to.option.post',
            ],
        ],
    ],
];
