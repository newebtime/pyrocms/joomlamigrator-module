<?php

namespace Newebtime\JoomlamigratorModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Illuminate\Database\MySqlConnection;
use Illuminate\Database\Schema\MySqlBuilder;
use Illuminate\Http\RedirectResponse;
use Newebtime\JoomlamigratorModule\Migrator\Migrator;

/**
 * Class MigratorController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class MigratorController extends AdminController
{
    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        if ((!$host = setting_value('newebtime.module.joomlamigrator::database_host'))
            || (!$name = setting_value('newebtime.module.joomlamigrator::database_name'))
            || (!$user = setting_value('newebtime.module.joomlamigrator::database_user'))
        ) {
            return $this->view->make('module::admin.index');
        };

        $prefix   = setting_value('newebtime.module.joomlamigrator::database_prefix');
        $password = setting('newebtime.module.joomlamigrator::database_password')
            ->getPresenter()->decrypt();

        config()->set('database.connections.joomla', [
            'driver'   => 'mysql',
            'host'     => $host,
            'database' => $name,
            'username' => $user,
            'password' => $password,
            'prefix'   => $prefix,
        ]);

        try {
            /**
             * Check the settings
             *
             * @var MySqlConnection $connection
             */
            $connection = \DB::connection('joomla');
            $connection->getPdo();

            /** @var MySqlBuilder $connection */
            $connection = \Schema::connection('joomla');

            if (!$hasUsers = $connection->hasTable('users')) {
                throw new \Exception('module::message.joomla_fail');
            }

            $hasMenus    = $connection->hasTable('menu');
            $hasContact  = $connection->hasTable('contact_details');
            $hasContent  = $connection->hasTable('content');
            $hasRedirect = $connection->hasTable('redirect_links');

            $joomla     = true;
        } catch (\PDOException $e) {
            $joomla     = false;
            $connection = false;
        } catch (\Exception $e) {
            $joomla     = false;
        }

        return $this->view->make('module::admin.index', [
            'settings'    => true,
            'connection'  => $connection,
            'joomla'      => $joomla,
            'hasUsers'    => $hasUsers ?? null,
            'hasMenus'    => $hasMenus ?? null,
            'hasContact'  => $hasContact ?? null,
            'hasContent'  => $hasContent ?? null,
            'hasRedirect' => $hasRedirect ?? null,
        ]);
    }

    /**
     * @param Migrator $migrator
     *
     * @return RedirectResponse
     */
    public function migrate(Migrator $migrator)
    {
        if (!$migrator->connect()) {
            return $this->redirect->to('admin/joomlamigration');
        }

        $migrator->launch();

        return back();
    }

    /**
     * @return \Illuminate\Contracts\View\View
     */
    public function help()
    {
        return $this->view->make('module::admin.help');
    }
}
