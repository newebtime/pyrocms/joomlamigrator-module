<?php

return [
    'database_host'        => [
        'name' => 'Host',
    ],
    'database_name'        => [
        'name' => 'Database Name',
    ],
    'database_prefix'      => [
        'name' => 'Tables Prefix',
    ],
    'database_user'        => [
        'name' => 'User',
    ],
    'database_password'    => [
        'name' => 'Password',
    ],
    'migrate_com_users'    => [
        'name'         => 'Users Component',
        'instructions' => 'Migrate Joomla! users and groups?',
        'warning'      => 'If disabled, all users references will be change to the connected user',
    ],
    'migrate_com_content'  => [
        'name'         => 'Content Component',
        'instructions' => 'Migrate contents?',
        'warning'      => 'If disabled, the "com_menus" will not migrate associated links',
    ],
    'migrate_com_contact'  => [
        'name'         => 'Contact Component',
        'instructions' => 'Migrate contact forms?',
    ],
    'migrate_com_menus'    => [
        'name'         => 'Menus Component',
        'instructions' => 'Migrate website menus and links?',
    ],
    'migrate_com_redirect' => [
        'name'         => 'Redirect Component',
        'instructions' => 'Migrate HTTP redirects?',
    ],
    'migrate_content_to'   => [
        'name'         => 'Migrate content to',
        'instructions' => '<strong>Automatic</strong> will try to define the module to use depending on "com_menus"',
        'option'       => [
            'automatic' => 'Automatic',
            'page'      => 'Pages Module',
            'post'      => 'Posts Module',
        ],
    ],
];
