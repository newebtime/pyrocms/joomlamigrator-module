<?php

return [
    'database_host'      => [
        'name' => 'Hôte',
    ],
    'database_name'      => [
        'name' => 'Nom de la base',
    ],
    'database_prefix'    => [
        'name' => 'Prefix des tables',
    ],
    'database_user'      => [
        'name' => 'Utilisateur',
    ],
    'database_password'  => [
        'name' => 'Mot de passe',
    ],
    'migrate_com_users'    => [
        'name'         => 'Users Component',
        'instructions' => 'Migrer les utilisateurs et groupes Joomla! ?',
        'warning'      => 'Si désactivé, les références aux utilisateurs seront changées pour l\'utilisateur actif',
    ],
    'migrate_com_content'  => [
        'name'         => 'Content Component',
        'instructions' => 'Migrer les pages de contenu ?',
        'warning'      => 'Si désactivé, the "com_menus" will not migrate associated links',
    ],
    'migrate_com_contact'  => [
        'name'         => 'Contact Component',
        'instructions' => 'Migrer les formulaires de contact ?',
    ],
    'migrate_com_menus'    => [
        'name'         => 'Menus Component',
        'instructions' => 'Migrer les menus et liens ?',
    ],
    'migrate_com_redirect' => [
        'name'         => 'Redirect Component',
        'instructions' => 'Migrer les redirections HTTP ?',
    ],
    'migrate_content_to' => [
        'name'         => 'Migrer le contenu en',
        'instructions' => '<strong>Automatique</strong> va essayer de definir le module en se basent sur le "com_menus"',
        'option'       => [
            'automatic' => 'Automatique',
            'page'      => 'Pages Module',
            'post'      => 'Posts Module',
        ],
    ],
];
