<?php

namespace Newebtime\JoomlamigratorModule\Http\Controller\Admin;

use Anomaly\SettingsModule\Setting\Form\SettingFormBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Anomaly\Streams\Platform\Support\Authorizer;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SettingsController
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class SettingsController extends AdminController
{
    /**
     * Return a form for editing settings.
     *
     * @param  SettingFormBuilder $form
     * @param  Authorizer $authorizer
     *
     * @return Response
     */
    public function edit(SettingFormBuilder $form, Authorizer $authorizer)
    {
        if (!$authorizer->authorize('anomaly.module.settings::settings.write')) {
            abort(403);
        }

        $form->setButtons([
            'cancel' => [
                'href' => 'admin/joomlamigrator'
            ]
        ]);

        return $form->render('newebtime.module.joomlamigrator');
    }
}
