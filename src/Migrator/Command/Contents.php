<?php

namespace Newebtime\JoomlamigratorModule\Migrator\Command;

use Anomaly\PagesModule\Page\Contract\PageRepositoryInterface;
use Anomaly\PagesModule\Type\Contract\TypeRepositoryInterface as PageType;
use Anomaly\PostsModule\Category\Contract\CategoryRepositoryInterface;
use Anomaly\PostsModule\Post\Contract\PostRepositoryInterface;
use Anomaly\PostsModule\Type\Contract\TypeRepositoryInterface as PostType;
use Anomaly\Streams\Platform\Message\MessageBag;
use Illuminate\Database\MySqlConnection;

/**
 * Class Contents
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class Contents
{
    /**
     * @var MySqlConnection
     */
    protected $connection;

    /**
     * @var MessageBag
     */
    protected $messages;

    /**
     * Users constructor.
     *
     * @param MySqlConnection $connection
     */
    public function __construct(MySqlConnection $connection)
    {
        $this->connection = $connection;
        $this->messages   = app('Anomaly\Streams\Platform\Message\MessageBag');
    }

    /**
     * Table: content
     *
     * @param PageRepositoryInterface     $pages
     * @param PostRepositoryInterface     $posts
     * @param CategoryRepositoryInterface $categories
     * @param PageType                    $typePages
     * @param PostType                    $typePosts
     */
    public function handle(
        PageRepositoryInterface $pages,
        PostRepositoryInterface $posts,
        CategoryRepositoryInterface $categories,
        PageType $typePages,
        PostType $typePosts
    ) {
        $pages->truncate();
        $posts->truncate();

        $jMenus = $this->connection->table('menu')->where('client_id', 0)->get();
        $idxs   = [];

        foreach ($jMenus as $jMenu) {
            if ($jMenu->link) {
                parse_str(parse_url($jMenu->link, PHP_URL_QUERY), $link);
                if (isset($link['view']) && $link['view'] == 'article') {
                    $idxs[] = $link['id'];
                }
            }
        }

        $typePage = $typePages->findBySlug('default');
        $typePost = $typePosts->findBySlug('default');
        $category = $categories->findBySlug('news');

        $jContents = $this->connection->table('content')->get();

        $migrateContent = setting_value('newebtime.module.joomlamigrator::migrate_content_to');

        foreach ($jContents as $jContent) {
            if ($migrateContent == 'page' || ($migrateContent == 'auto' && in_array($jContent->id, $idxs))) {
                if ($pages->find($jContent->id)) {
                    $this->messages->warning(trans('module::message.page_exist'));

                    continue;
                }

                $pages->create([
                    'title'            => $jContent->title,
                    'slug'             => $jContent->alias,
                    'type'             => $typePage,
                    'meta_title'       => $jContent->metakey,
                    'meta_description' => $jContent->metadesc,
                    'theme_layout'     => 'theme::layouts/default.twig',
                ]);
            } elseif ($migrateContent == 'post' || ($migrateContent == 'auto' || !in_array($jContent->id, $idxs))) {
                if ($posts->find($jContent->id)) {
                    $this->messages->warning(trans('module::message.post_exist'));

                    continue;
                }

                $posts->create([
                    'title'            => $jContent->title,
                    'slug'             => $jContent->alias,
                    'type'             => $typePost,
                    'publish_at'       => $jContent->publish_up,
                    'author'           => 1,
                    'category'         => $category,
                    'meta_title'       => $jContent->metakey,
                    'meta_description' => $jContent->metadesc,
                    'featured'         => $jContent->featured,
                ]);
            }
        }
    }
}
