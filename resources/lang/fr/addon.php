<?php

return [
    'title'       => 'Joomla! Migrator',
    'name'        => 'Joomla! Migrator Module',
    'description' => 'Migrer un base de donnée Joomla! sur PyroCMS'
];
