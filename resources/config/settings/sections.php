<?php

return [
    [
        'tabs' => [
            'database'   => [
                'title'  => 'newebtime.module.joomlamigrator::tab.database',
                'fields' => [
                    'database_host',
                    'database_name',
                    'database_prefix',
                    'database_user',
                    'database_password',
                ],
            ],
            'components' => [
                'title'  => 'newebtime.module.joomlamigrator::tab.components',
                'fields' => [
                    'migrate_com_users',
                    'migrate_com_content',
                    'migrate_com_contact',
                    'migrate_com_menus',
                    'migrate_com_redirect',
                ],
            ],
            'options'    => [
                'title'  => 'newebtime.module.joomlamigrator::tab.options',
                'fields' => [
                    'migrate_content_to',
                ],
            ],
        ],
    ],
];
