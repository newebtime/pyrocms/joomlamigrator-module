<?php

return [
    'title'       => 'Joomla! Migrator',
    'name'        => 'Joomla! Migrator Module',
    'description' => 'Migrate a Joomla! database to PyroCMS'
];
