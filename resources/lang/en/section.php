<?php

return [
    'migrator' => [
        'title' => 'Migrator',
    ],
    'help' => [
        'title' => 'Help',
    ],
    'settings' => [
        'title' => 'Settings',
    ],
];
