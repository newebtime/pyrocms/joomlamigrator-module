<?php

namespace Newebtime\JoomlamigratorModule\Migrator;

use Illuminate\Database\MySqlConnection;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class Migrator
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class Migrator
{
    use DispatchesJobs;

    /**
     * @var MySqlConnection
     */
    protected $connection;

    /**
     * @return boolean
     */
    public function connect()
    {
        return $this->makeConnection();
    }

    public function launch()
    {
        if (setting_value('newebtime.module.joomlamigrator::migrate_com_users')) {
            $this->dispatch(new Command\Users($this->connection));
            $this->dispatch(new Command\Groups($this->connection));
            $this->dispatch(new Command\UserGroups($this->connection));
            $this->dispatch(new Command\UserFields($this->connection));
        }

        if (setting_value('newebtime.module.joomlamigrator::migrate_com_content')) {
            $this->dispatch(new Command\Contents($this->connection));
        }

        if (setting_value('newebtime.module.joomlamigrator::migrate_com_contact') &&
            app('module.collection')->get('anomaly.module.forms')
        ) {
            $this->dispatch(new Command\Contacts($this->connection));
        }

        if (setting_value('newebtime.module.joomlamigrator::migrate_com_menus')) {
            $this->dispatch(new Command\Menus($this->connection));
        }

        if (setting_value('newebtime.module.joomlamigrator::migrate_com_redirect')) {
            $this->dispatch(new Command\Redirects($this->connection));
        }
    }

    /**
     * @return boolean
     */
    protected function makeConnection()
    {
        if ((!$host = setting_value('newebtime.module.joomlamigrator::database_host'))
            || (!$name = setting_value('newebtime.module.joomlamigrator::database_name'))
            || (!$user = setting_value('newebtime.module.joomlamigrator::database_user'))
        ) {
            return false;
        };

        $prefix   = setting_value('newebtime.module.joomlamigrator::database_prefix');
        $password = setting('newebtime.module.joomlamigrator::database_password')
            ->getPresenter()->decrypt();

        config()->set('database.connections.joomla', [
            'driver'   => 'mysql',
            'host'     => $host,
            'database' => $name,
            'username' => $user,
            'password' => $password,
            'prefix'   => $prefix,
        ]);

        try {
            $this->connection = \DB::connection('joomla');
            $this->connection->getPdo();
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}
