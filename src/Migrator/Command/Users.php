<?php

namespace Newebtime\JoomlamigratorModule\Migrator\Command;

use Anomaly\PreferencesModule\Preference\Contract\PreferenceRepositoryInterface;
use Anomaly\Streams\Platform\Message\MessageBag;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Illuminate\Database\MySqlConnection;

/**
 * Class Users
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class Users
{
    /**
     * @var MySqlConnection
     */
    protected $connection;

    /**
     * @var MessageBag
     */
    protected $messages;

    /**
     * Users constructor.
     *
     * @param MySqlConnection $connection
     */
    public function __construct(MySqlConnection $connection)
    {
        $this->connection = $connection;
        $this->messages   = app('Anomaly\Streams\Platform\Message\MessageBag');
    }

    /**
     * Component    com_users
     * Table        users
     *
     * Uncompatible fields
         * sendEmail
         * lastResetTime
         * resetCount
         * optKey
         * otep
         * requireReset
         * password (Need to reset)
     *
     * @param UserRepositoryInterface       $users
     * @param PreferenceRepositoryInterface $preferences
     */
    public function handle(UserRepositoryInterface $users, PreferenceRepositoryInterface $preferences)
    {
        $jUsers = $this->connection->table('users')->get();

        foreach ($jUsers as $jUser) {
            if ($users->find($jUser->id)) {
                $this->messages->warning(trans('module::message.user_exist', ['id' => $jUser->id]));

                continue;
            } elseif ($users->findByEmail($jUser->email)) {
                $this->messages->warning(trans('module::message.email_already_use', ['email' => $jUser->email]));

                continue;
            } elseif ($users->findByUsername($jUser->username)) {
                $this->messages->warning(
                    trans('module::message.username_already_use', ['username' => $jUser->username])
                );

                continue;
            }

            $user = $users->create([
                'id'            => $jUser->id,
                'display_name'  => $jUser->name,
                'username'      => $jUser->username,
                'email'         => $jUser->email,
                'enabled'       => !$jUser->block,
                'created_at'    => $jUser->registerDate,
                'last_login_at' => $jUser->lastvisitDate,
                'activated'     => !$jUser->activation,
            ]);

            $params = json_decode($jUser->params);

            if ($params && $params->timezone) {
                $preferences->create([
                    'user_id' => $user->getId(),
                    'key'     => 'streams::timezone',
                    'value'   => $params->timezone
                ]);
            }
        }
    }
}
