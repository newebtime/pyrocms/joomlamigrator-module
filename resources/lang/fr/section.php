<?php

return [
    'migrator' => [
        'title' => 'Migrateurr',
    ],
    'help' => [
        'title' => 'Aide',
    ],
    'settings' => [
        'title' => 'Paramètres',
    ],
];
