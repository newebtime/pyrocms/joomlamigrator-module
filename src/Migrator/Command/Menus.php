<?php

namespace Newebtime\JoomlamigratorModule\Migrator\Command;

use Anomaly\NavigationModule\Link\Contract\LinkRepositoryInterface;
use Anomaly\NavigationModule\Menu\Contract\MenuRepositoryInterface;
use Anomaly\PageLinkTypeExtension\PageLinkTypeModel;
use Anomaly\Streams\Platform\Entry\EntryRepository;
use Anomaly\Streams\Platform\Message\MessageBag;
use Anomaly\UrlLinkTypeExtension\UrlLinkTypeModel;
use Illuminate\Database\MySqlConnection;

/**
 * Class Menus
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class Menus
{
    /**
     * @var MySqlConnection
     */
    protected $connection;

    /**
     * @var MessageBag
     */
    protected $messages;

    /**
     * Users constructor.
     *
     * @param MySqlConnection $connection
     */
    public function __construct(MySqlConnection $connection)
    {
        $this->connection = $connection;
        $this->messages   = app('Anomaly\Streams\Platform\Message\MessageBag');
    }

    /**
     * Table: menu
     *
     * @param MenuRepositoryInterface $menus
     * @param LinkRepositoryInterface $links
     */
    public function handle(MenuRepositoryInterface $menus, LinkRepositoryInterface $links)
    {
        $menus->truncate();

        $jMenuTypes = $this->connection
            ->table('menu_types')
            ->where('client_id', 0)
            ->get();

        foreach ($jMenuTypes as $jMenuType) {
            if ($menus->find($jMenuType->id)) {
                $this->messages->warning(trans('module::message.menu_exist', ['id' => $jMenuType->id]));

                continue;
            }

            if ($menus->findBySlug($jMenuType->menutype)) {
                $this->messages->warning(trans('module::message.slug_already_use', ['slug' => $jMenuType->menutype]));

                continue;
            }

            $menus->create([
                'id'          => $jMenuType->id,
                'name'        => $jMenuType->title,
                'slug'        => $jMenuType->menutype,
                'description' => $jMenuType->description
            ]);
        }

        $pageModel = new EntryRepository();
        $pageLink  = $pageModel->setModel(new PageLinkTypeModel());
        $urlModel  = new EntryRepository();
        $urlLink   = $urlModel->setModel(new UrlLinkTypeModel());

        $urlLink->truncate();
        $pageLink->truncate();
        $links->truncate();

        $jMenus = $this->connection
            ->table('menu')
            ->where('client_id', 0)
            ->where('menutype', '!=', '')
            ->get();

        foreach ($jMenus as $jMenu) {
            if ($links->find($jMenu->id)) {
                $this->messages->warning(trans('module::message.link_exist', ['id' => $jMenu->id]));

                continue;
            }
            $menu = $menus->findBySlug($jMenu->menutype);

            if ($jMenu->type == 'url') {
                $url = $urlLink->create([
                    'title' => $jMenu->title,
                    'url'   => $jMenu->link
                ]);

                $links->create([
                    'menu'   => $menu,
                    'type'   => 'anomaly.extension.url_link_type',
                    'target' => '_blank',
                    'entry'  => $url
                ]);
            } else {
                parse_str(parse_url($jMenu->link, PHP_URL_QUERY), $link);

                if (isset($link['view']) && $link['view'] == 'article') {
                    $page = $pageLink->create([
                        'title'   => $jMenu->title,
                        'page_id' => $link['id']
                    ]);

                    $links->create([
                        'menu'   => $menu,
                        'type'   => 'anomaly.extension.page_link_type',
                        'target' => '_blank',
                        'entry'  => $page
                    ]);
                } else {
                    $url = $urlLink->create([
                        'title' => $jMenu->title,
                        'url'   => $jMenu->path
                    ]);

                    $links->create([
                        'menu'   => $menu,
                        'type'   => 'anomaly.extension.url_link_type',
                        'target' => '_blank',
                        'entry'  => $url
                    ]);
                }
            }
        }
    }
}
