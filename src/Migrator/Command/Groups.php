<?php

namespace Newebtime\JoomlamigratorModule\Migrator\Command;

use Anomaly\Streams\Platform\Message\MessageBag;
use Anomaly\UsersModule\Role\Contract\RoleRepositoryInterface;
use Illuminate\Database\MySqlConnection;

/**
 * Class Groups
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class Groups
{
    /**
     * @var MySqlConnection
     */
    protected $connection;

    /**
     * @var MessageBag
     */
    protected $messages;

    /**
     * Users constructor.
     *
     * @param MySqlConnection $connection
     */
    public function __construct(MySqlConnection $connection)
    {
        $this->connection = $connection;
        $this->messages   = app('Anomaly\Streams\Platform\Message\MessageBag');
    }

    /**
     * Table: usergroups
     *
     * @param RoleRepositoryInterface $roles
     */
    public function handle(RoleRepositoryInterface $roles)
    {
        $jGroups = $this->connection->table('usergroups')->get();

        foreach ($jGroups as $jGroup) {
            if ($roles->findBySlug(str_replace(" ", "_", strtolower($jGroup->title)))) {
                $this->messages->warning(trans('module::message.role_already_use', ['role' => $jGroup->title]));

                continue;
            }

            $roles->create([
                'name' => $jGroup->title
            ]);
        }

        $jViewLevels = $this->connection->table('viewlevels')->get();

        foreach ($jViewLevels as $jViewLevel) {
            if ($roles->findBySlug(str_replace(" ", "_", strtolower($jViewLevel->title)))) {
                $this->messages->warning(trans('module::message.role_already_use', ['role' => $jViewLevel->title]));

                continue;
            }

            $roles->create([
                'name' => $jViewLevel->title
            ]);
        }
    }
}
