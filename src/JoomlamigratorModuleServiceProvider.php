<?php

namespace Newebtime\JoomlamigratorModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Newebtime\JoomlamigratorModule\Migrator\Migrator;

/**
 * Class JoomlamigratorModuleServiceProvider
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class JoomlamigratorModuleServiceProvider extends AddonServiceProvider
{
    /**
     * {@inheritdoc}
     */
    protected $bindings = [
        Migrator::class => Migrator::class,
    ];

    /**
     * {@inheritdoc}
     */
    protected $routes = [
        'admin/joomlamigrator'          => [
            'uses' => 'Newebtime\JoomlamigratorModule\Http\Controller\Admin\MigratorController@index'
        ],
        'admin/joomlamigrator/migrate'  => [
            'uses' => 'Newebtime\JoomlamigratorModule\Http\Controller\Admin\MigratorController@migrate',
            'as'   => 'newebtime.module.joomlamigrator::admin.migrate',
        ],
        'admin/joomlamigrator/help'     => [
            'uses' => 'Newebtime\JoomlamigratorModule\Http\Controller\Admin\MigratorController@help'
        ],
        'admin/joomlamigrator/settings' => [
            'uses' => 'Newebtime\JoomlamigratorModule\Http\Controller\Admin\SettingsController@edit',
        ],
    ];
}
