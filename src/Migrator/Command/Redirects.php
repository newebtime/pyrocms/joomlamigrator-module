<?php

namespace Newebtime\JoomlamigratorModule\Migrator\Command;

use Anomaly\RedirectsModule\Redirect\Contract\RedirectRepositoryInterface;
use Anomaly\Streams\Platform\Message\MessageBag;
use Illuminate\Database\MySqlConnection;

/**
 * Class Redirects
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class Redirects
{
    /**
     * @var MySqlConnection
     */
    protected $connection;

    /**
     * @var MessageBag
     */
    protected $messages;

    /**
     * Users constructor.
     *
     * @param MySqlConnection $connection
     */
    public function __construct(MySqlConnection $connection)
    {
        $this->connection = $connection;
        $this->messages   = app('Anomaly\Streams\Platform\Message\MessageBag');
    }

    /**
     * Component    com_redirect
     * Table        redirect_links
     *
     * Uncompatible fields
         * referer
         * comment
         * hits
         * published (unpublished will not be migrated)
         * header
     *
     * @param RedirectRepositoryInterface $redirects
     */
    public function handle(RedirectRepositoryInterface $redirects)
    {
        $redirects->truncate();

        $jRedirects = $this->connection
            ->table('redirect_links')
            ->where('published', 1)
            ->get();

        foreach ($jRedirects as $jRedirect) {
            if ($redirects->find($jRedirect->id)) {
                $this->messages->warning(trans('module::message.redirect_exist', ['id' => $jRedirect->id]));

                continue;
            }

            $redirects->create([
                'from'       => $jRedirect->old_url,
                'to'         => $jRedirect->new_url,
                'created_at' => $jRedirect->created_date,
                'updated_at' => $jRedirect->modified_date,
                'status'     => '301',
            ]);
        }
    }
}
