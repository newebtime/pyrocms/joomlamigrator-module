<?php

namespace Newebtime\JoomlamigratorModule\Migrator\Command;

use Anomaly\Streams\Platform\Assignment\Contract\AssignmentRepositoryInterface;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;
use Anomaly\Streams\Platform\Message\MessageBag;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;
use Illuminate\Database\MySqlConnection;

/**
 * Class UserFields
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class UserFields
{
    /**
     * @var MySqlConnection
     */
    protected $connection;

    /**
     * @var MessageBag
     */
    protected $messages;

    /**
     * Users constructor.
     *
     * @param MySqlConnection $connection
     */
    public function __construct(MySqlConnection $connection)
    {
        $this->connection = $connection;
        $this->messages   = app('Anomaly\Streams\Platform\Message\MessageBag');
    }

    /**
     * Table: fields
     *
     * @param FieldRepositoryInterface      $fields
     * @param StreamRepositoryInterface     $stream
     * @param AssignmentRepositoryInterface $assignments
     */
    public function handle(
        FieldRepositoryInterface $fields,
        StreamRepositoryInterface $stream,
        AssignmentRepositoryInterface $assignments
    ) {
        $jFields = $this->connection->table('fields')->where('state', 1)->where('context', 'com_users.user')->get();
        $stream  = $stream->findBySlugAndNamespace('users', 'users');

        foreach ($jFields as $jField) {
            if ($fields->findBy('slug', $jField->name)) {
                $this->messages->warning(trans('module::message.field_already_use', ['field' => $jField->name]));

                continue;
            }

            $options = json_decode($jField->params);
            $fieldParams = json_decode($jField->fieldparams);

            if ($jField->type == 'text') {
                $type   = 'anomaly.field_type.text';
                $config = [
                    'type'          => $fieldParams->filter && $fieldParams->filter == 'tel' ? 'tel' : 'text',
                    'max'           => $fieldParams->maxlength ? $fieldParams->maxlength : '',
                    'default_value' => $jField->default_value
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'calendar') {
                $type   = 'anomaly.field_type.datetime';
                $config = [
                    'mode'        => 'datetime',
                    'date_format' => 'm/d/Y',
                    'time_format' => 'g:i A',
                    'step'        => 1
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'checkboxes') {
                $type          = 'anomaly.field_type.checkboxes';
                $configOptions = '';

                foreach ($fieldParams->options as $option) {
                    $configOptions .= $option->value . ': ' . $option->name . "\n";
                }

                $config = [
                    'mode'          => 'checkboxes',
                    'options'       => $configOptions,
                    'default_value' => $jField->default_value,
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'color') {
                $type   = 'anomaly.field_type.colorpicker';
                $config = [
                    'default_value' => $jField->default_value
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'editor') {
                $type   = 'anomaly.field_type.wysiwyg';
                $config = [
                    'default_value' => $jField->default_value,
                    'height'        => $fieldParams->height
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'integer') {
                $type   = 'anomaly.field_type.integer';
                $config = [
                    'default_value' => $jField->default_value,
                    'min'           => $fieldParams->first,
                    'max'           => $fieldParams->last,
                    'step'          => $fieldParams->step
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'list') {
                $type   = 'anomaly.field_type.select';
                $configOptions = '';
                foreach ($fieldParams->options as $option) {
                    $configOptions .= $option->value . ':' . $option->name . "\n";
                }
                $config = [
                    'mode'          => 'dropdown',
                    'options'       => $configOptions,
                    'default_value' => $jField->default_value,
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'imagelist') {
                $type   = 'anomaly.field_type.files';
                $config = [
                    'mode' => 'default'
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'media') {
                $type   = 'anomaly.field_type.file';
                $config = [
                    'mode' => 'default'
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'radio') {
                $type   = 'anomaly.field_type.select';
                $configOptions = '';
                foreach ($fieldParams->options as $option) {
                    $configOptions .= $option->value . ':' . $option->name . "\n";
                }
                $config = [
                    'mode'          => 'radio',
                    'options'       => $configOptions,
                    'default_value' => $jField->default_value,
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'textarea') {
                $type   = 'anomaly.field_type.textarea';
                $config = [
                    'default_value' => $jField->default_value,
                    'rows'          => $fieldParams->rows,
                    'max'           => $fieldParams->maxlength,
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'url') {
                $type   = 'anomaly.field_type.url';
                $config = [
                    'default_value' => $jField->default_value
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'user') {
                $type   = 'anomaly.field_type.relationship';
                $config = [
                    'related' => 'Anomaly\Streams\Platform\Model\Users\UsersUsersEntryModel',
                    'mode'    => 'lookup'
                ];
                $config = json_encode($config);
            } else if ($jField->type == 'usergrouplist') {
                $type   = 'anomaly.field_type.relationship';
                $config = [
                    'related' => 'Anomaly\Streams\Platform\Model\Users\UsersRolesEntryModel',
                    'mode'    => 'lookup'
                ];
                $config = json_encode($config);
            } else {
                $type   = '';
                $config = '';
            }

            $field = $fields->create([
                'name'          => $jField->title,
                'slug'          => $jField->name,
                'placeholder'   => $options->hint,
                'instructions'  => $jField->description,
                'namespace'     => 'users',
                'type'          => $type,
                'config'        => $config,
            ]);

            if ($assignments->findBy('field_id', $field->getId())) {
                $this->messages->warning(trans('module::message.assignment_exist'));

                continue;
            }

            $assignments->create([
                'stream' => $stream,
                'field'  => $field,
                'required'  => $jField->required
            ]);
        }
    }
}
