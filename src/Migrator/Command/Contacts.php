<?php

namespace Newebtime\JoomlamigratorModule\Migrator\Command;

use Anomaly\FormsModule\Form\Contract\FormRepositoryInterface;
use Anomaly\Streams\Platform\Message\MessageBag;
use Illuminate\Database\MySqlConnection;

/**
 * Class Contacts
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class Contacts
{
    /**
     * @var MySqlConnection
     */
    protected $connection;

    /**
     * @var MessageBag
     */
    protected $messages;

    /**
     * Users constructor.
     *
     * @param MySqlConnection $connection
     */
    public function __construct(MySqlConnection $connection)
    {
        $this->connection = $connection;
        $this->messages   = app('Anomaly\Streams\Platform\Message\MessageBag');
    }

    /**
     * Table: contact_details
     *
     * @param FormRepositoryInterface $forms
     */
    public function handle(FormRepositoryInterface $forms)
    {
        $forms->truncate();

        $jContacts = $this->connection->table('contact_details')->get();

        foreach ($jContacts as $jContact) {
            if ($forms->find($jContact->id)) {
                $this->messages->warning(trans('module::message.form_exist', ['id' => $jContact->id]));

                continue;
            }

            $forms->create([
                'id'           => $jContact->id,
                'form_name'    => $jContact->name,
                'form_slug'    => $jContact->alias,
                'form_handler' => 'anomaly.extension.standard_form',
            ]);
        }
    }
}
