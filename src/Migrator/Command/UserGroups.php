<?php

namespace Newebtime\JoomlamigratorModule\Migrator\Command;

use Anomaly\Streams\Platform\Message\MessageBag;
use Anomaly\UsersModule\Role\Contract\RoleRepositoryInterface;
use Anomaly\UsersModule\Role\RoleModel;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Anomaly\UsersModule\User\UserModel;
use Illuminate\Database\MySqlConnection;

/**
 * Class UserGroups
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class UserGroups
{
    /**
     * @var MySqlConnection
     */
    protected $connection;

    /**
     * @var MessageBag
     */
    protected $messages;

    /**
     * Users constructor.
     *
     * @param MySqlConnection $connection
     */
    public function __construct(MySqlConnection $connection)
    {
        $this->connection = $connection;
        $this->messages   = app('Anomaly\Streams\Platform\Message\MessageBag');
    }

    /**
     * Table: user_usergroup_map
     *
     * @param UserRepositoryInterface $users
     * @param RoleRepositoryInterface $roles
     */
    public function handle(UserRepositoryInterface $users, RoleRepositoryInterface $roles)
    {
        $jUserGroups = $this->connection->table('user_usergroup_map')->get();

        foreach ($jUserGroups as $jUserGroup) {
            /** @var UserModel $user */
            if (!$user = $users->find($jUserGroup->user_id)) {
                $this->messages->warning(trans('module::message.user_not_found', ['id' => $jUserGroup->user_id]));

                continue;
            }

            if (!$group = $this->connection->table('usergroups')->find($jUserGroup->group_id)) {
                $this->messages->warning(trans('module::message.group_not_found', ['id' => $jUserGroup->group_id]));

                continue;
            }

            /** @var RoleModel $role */
            if (!$role = $roles->findBySlug(str_replace(" ", "_", strtolower($group->title)))) {
                $this->messages->warning(trans('module::message.role_not_found', ['role' => $group->title]));

                continue;
            }

            if ($user->roles()->find($role->id)) {
                continue;
            }

            $user->roles()->attach($role->id);
        }
    }
}
