# Joomla! Migrator Module

Migrate a Joomla! database to PyroCMS. Keep in mind a lot of things are managed completly differently and will need
to be reworked. This module is here just to fasten the migration a bit.

> **Warning:** Please use this migrator on a fresh installation of PyroCMS. Many tables will be reseted.


## Requirement

* Joomla 3.8.x
* PyroCMS 3.5.x

## Installation

* On a fresh PyroCMS installation, install this module
* Configure the Joomla! Database connection in the Settings
* Launch the migration
* That it!

## Limitation

* For now on it is limited to native Joomla! component compatible with native PyroCMS component.
* Medias will not be migrated (Joomla! do not store medias in DB), but you can just copy/paste them.

### Compatible components

* com_users
* com_menus
* com_content
* com_redirect
* com_contact (required Pro addon: `Form Module`)

### Non-compatible components

* com_banners
* com_finder
* com_messages
* com_newsfeeds
* com_search
* com_tags
